export default function Hobbies() {
    return (
        <>
    <div className="text-center">
    <h1>My Hobbies</h1>
    <p>I have some hobbies which include Videogames and learning about technology.</p>
    <p>Currently been learning more about Java and making small personal projects.</p>
    <p>I usually stay up to date with technology expecially news related to <a href="https://9to5mac.com/">Apple</a>, Microsoft, and Tesla from a bunch of websites. <br /> Including:</p>

        <ol className="list-group list-group-numbered col-lg-2 col-md-8 col-sm-12 mx-auto">
            <li className="list-group-item d-flex justify-content-between align-items-start"><a className="ms-2 fw-bold" href="https://9to5mac.com/">9to5mac</a></li>
            <li className="list-group-item d-flex justify-content-between align-items-start"><a className="ms-2 fw-bold" href="https://macromurs.com/">Macromurs</a></li>
            <li className="list-group-item d-flex justify-content-between align-items-start"><a className="ms-2 fw-bold" href="https://techcrunch.com/">Techcrunch</a></li>
            <li className="list-group-item d-flex justify-content-between align-items-start"><a className="ms-2 fw-bold" href="https://www.cnet.com/">cnet</a></li>

    </ol>

    </div>
        </>
    )
};
